import "bootstrap/dist/css/bootstrap.min.css";

import {Browser, BrowserRouter, Route, Switch} from "react-router-dom"

import {CustomNavbar} from "./components/navbar/Navbar";
import { MedicoList } from "./components/medico/MedicoList";

function App() {
  return (
    <BrowserRouter>
      <CustomNavbar />
      <div className="container my-4">
        <Switch>
          <Route path="/" component={<MedicoList />} />
          <Route path="/" component={MedicoForm} />
        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;
