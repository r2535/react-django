import {
  Button,
  Card,
  CardBody,
  CardText,
  CardTitle,
} from "reactstrap";

export function MedicoItem({ medico }) {
  return (
    <div className="col-md-4">
      <Card>
        <CardBody>
          <CardTitle tag="h5">
            {medico.nombre} {medico.apellido}
          </CardTitle>
          <CardText>{medico.correo}</CardText>
          <Button>Button</Button>
        </CardBody>
      </Card>
    </div>
  );
}
