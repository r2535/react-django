import {Form, FormGroup,Label, Col, Input, FormGroup} from "reacts"

export function MedicoForm(){

    return (
      <div>
        <Form>
          <FormGroup row>
            <Label for="fechaRegistro" sm={2}>
              Fecha Registro
            </Label>
            <Col sm={10}>
              <Input id="fechaRegistro" name="fechaRegistro" type="date" />
            </Col>
          </FormGroup>
          <FormGroup row>
            <Label for="fechaModificacion" sm={2}>
              Fecha Modificacion
            </Label>
            <Col sm={10}>
              <Input
                id="fechaModificacion"
                name="fechaModificacion"
                type="date"
              />
            </Col>
          </FormGroup>
          <FormGroup row>
            <Label for="usuarioRegistro" sm={2}>
              Usuario Registro
            </Label>
            <Col sm={10}>
              <Input id="usuarioRegistro" name="usuarioRegistro" type="text" />
            </Col>
          </FormGroup>
          <FormGroup row>
            <Label for="usuarioModificacion" sm={2}>
              Usuario Modificacion
            </Label>
            <Col sm={10}>
              <Input
                id="usuarioModificacion"
                name="usuarioModificacion"
                type="text"
              />
            </Col>
          </FormGroup>

          <FormGroup row>
            <Label for="nombre" sm={2}>
              Nombre:
            </Label>
            <Col sm={10}>
              <Input id="nombre" name="nombre" type="text" />
            </Col>
          </FormGroup>
          <FormGroup row>
            <Label for="apellido" sm={2}>
              Apellido:
            </Label>
            <Col sm={10}>
              <Input id="apellido" name="apellido" type="text" />
            </Col>
          </FormGroup>
          <FormGroup row>
            <Label for="dni" sm={2}>
              dni:
            </Label>
            <Col sm={10}>
              <Input id="dni" name="dni" type="number" />
            </Col>
          </FormGroup>

          <FormGroup row>
            <Label for="sexo" sm={2}>
              Sexo:
            </Label>
            <Col sm={10}>
              <Input id="sexo" multiple name="sexo" type="select">
                <option>Femenino</option>
                <option>Masculino</option>
              </Input>
            </Col>
          </FormGroup>
          <FormGroup row>
            <Label for="direccion" sm={2}>
              Direccion:
            </Label>
            <Col sm={10}>
              <Input id="direccion" name="direccion" type="textarea" />
            </Col>
          </FormGroup>
          <FormGroup row>
            <Label for="correo" sm={2}>
              E-mail:
            </Label>
            <Col sm={10}>
              <Input id="correo" name="correo" type="Email" />
            </Col>
          </FormGroup>
          <FormGroup row>
            <Label for="telefono" sm={2}>
              Telefono::
            </Label>
            <Col sm={10}>
              <Input id="telefono" name="telefono" type="number" />
            </Col>
          </FormGroup>
          <FormGroup row>
            <Label for="colegiatura" sm={2}>
              Colegiatura:
            </Label>
            <Col sm={10}>
              <Input id="colegiatura" name="colegiatura" type="number" />
            </Col>
          </FormGroup>
          <FormGroup row>
            <Label for="fechaNacimiento" sm={2}>
              Fecha Nacimiento:
            </Label>
            <Col sm={10}>
              <Input id="fechaNacimiento" name="fechaNacimiento" type="date" />
            </Col>
          </FormGroup>
         
          <FormGroup check row>
            <Col
              sm={{
                offset: 2,
                size: 10,
              }}
            >
              <Button>Submit</Button>
            </Col>
          </FormGroup>
        </Form>
      </div>
    );
}