import * as MedicoServer from "./MedicoServer";

import { useEffect, useState } from "react";

import {MedicoItem} from "./MedicoItem"

export function MedicoList() {
  const [medicos, setMedicos] = useState([]);

  const listMedicos = async () => {
    try {
      const res = await MedicoServer.ListMedicos();
      const data = await res.json();
      console.log(data);
      setMedicos(data.results);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    listMedicos();
  }, []);

  return (
    <div className="row">
      {medicos.map((medico) =>(
        <MedicoItem key={medico.id} medico={medico}/>
))}
    </div>
  );
}
