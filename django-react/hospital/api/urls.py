from rest_framework import routers
from api.views import MedicoViewSet, EspecialidadViewSet, MedicosEspecialidadViewSet, PacienteViewSet, CitaViewSet, HorarioViewSet
from django.urls import path, include

router = routers.DefaultRouter()
router.register('medico', MedicoViewSet)
router.register('especialidad', EspecialidadViewSet)
router.register('medicoespecialidad', MedicosEspecialidadViewSet)
router.register('paciente', PacienteViewSet)
router.register('cita', CitaViewSet)
router.register('horario', HorarioViewSet)

urlpatterns = [
    path('', include(router.urls)),
]

