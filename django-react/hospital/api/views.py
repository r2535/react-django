from rest_framework import viewsets

from api.serializer import MedicoSerializer, EspecialidadSerializer, MedicosEspecialidadSerializer, PacienteSerializer, \
    CitaSerializer, HorarioSerializer
from modulo.models import Medico, Especialidad, MedicosEspecialidad, Paciente, Cita, Horario


class MedicoViewSet(viewsets.ModelViewSet):
    queryset = Medico.objects.all()
    serializer_class = MedicoSerializer


class EspecialidadViewSet(viewsets.ModelViewSet):
    queryset = Especialidad.objects.all()
    serializer_class = EspecialidadSerializer


class MedicosEspecialidadViewSet(viewsets.ModelViewSet):
    queryset = MedicosEspecialidad.objects.all()
    serializer_class = MedicosEspecialidadSerializer


class PacienteViewSet(viewsets.ModelViewSet):
    queryset = Paciente.objects.all()
    serializer_class = PacienteSerializer


class CitaViewSet(viewsets.ModelViewSet):
    queryset = Cita.objects.all()
    serializer_class = CitaSerializer


class HorarioViewSet(viewsets.ModelViewSet):
    queryset = Horario.objects.all()
    serializer_class = HorarioSerializer
