from rest_framework import serializers

from modulo.models import Medico, Especialidad, MedicosEspecialidad, Paciente, Cita


class MedicoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Medico
        fields = '__all__'


class EspecialidadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Especialidad
        fields = '__all__'


class MedicosEspecialidadSerializer(serializers.ModelSerializer):
    class Meta:
        model = MedicosEspecialidad
        fields = '__all__'


class PacienteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Paciente
        fields = '__all__'


class CitaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cita
        fields = '__all__'


class HorarioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cita
        fields = '__all__'













