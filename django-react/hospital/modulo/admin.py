from django.contrib import admin

# Register your models here.
from modulo.models import Medico, Especialidad, MedicosEspecialidad, Paciente, Cita, Horario


@admin.register(Medico)
class MedicoAdmin(admin.ModelAdmin):
    list_display = (
        'nombre',
        'apellido',
    )

@admin.register(Especialidad)
class EspecialidadAdmin(admin.ModelAdmin):
    list_display = (
        'nombre',
    )


@admin.register(MedicosEspecialidad)
class MedicosEspecialiadadAdmin(admin.ModelAdmin):
    list_display = (
        'medico_id',
        'especialidad_id',
    )


@admin.register(Paciente)
class PacienteAdmin(admin.ModelAdmin):
    list_display = (
        'nombre',
        'apellido',
    )


@admin.register(Cita)
class CitaAdmin(admin.ModelAdmin):
    list_display = (
        'medico_id',
        'paciente_id',
        'final_atencion',
    )


@admin.register(Horario)
class HorarioAdmin(admin.ModelAdmin):
    list_display = (
        'medico_id',
        'fecha_atencion',

    )