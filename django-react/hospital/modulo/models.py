from django.db import models

# Create your models here.


class CommonInfo(models.Model):
    fecha_registro = models.DateField(auto_now_add=True, null=True)
    fecha_modificacion = models.DateField(auto_now=True, null=True)
    usuario_registro = models.CharField(max_length=50, null=True)
    usuario_modificacion = models.CharField(max_length=100, null=True)
    activo = models.BooleanField(default=False, null=True)

    class Meta:
        abstract = True


class Medico(CommonInfo):
    nombre = models.CharField(max_length=150)
    apellido = models.CharField(max_length=150)
    dni = models.CharField(max_length=150)
    direccion = models.CharField(max_length=150)
    correo = models.EmailField(max_length=150)
    telefono = models.IntegerField()
    sexo = models.IntegerField()
    numero_colegiatura = models.IntegerField()
    fecha_nacimiento = models.DateField()

    def __str__(self):
        return f'{self.nombre} {self.apellido}'


class Especialidad(CommonInfo):
    nombre = models.CharField(max_length=150)
    descripcion = models.TextField()

    def __str__(self):
        return self.nombre



class MedicosEspecialidad(CommonInfo):
    medico_id = models.ForeignKey(Medico, on_delete=models.CASCADE)
    especialidad_id = models.ForeignKey(Especialidad, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.medico_id} {self.especialidad_id}'


class Paciente(CommonInfo):
    nombre = models.CharField(max_length=150)
    apellido = models.CharField(max_length=150)
    dni = models.CharField(max_length=150)
    direccion = models.CharField(max_length=150)
    telefono = models.IntegerField()
    sexo = models.IntegerField()

    def __str__(self):
        return f'{self.nombre} {self.apellido}'



class Cita(CommonInfo):
    medico_id = models.ForeignKey(Medico, on_delete=models.CASCADE)
    paciente_id = models.ForeignKey(Paciente, on_delete=models.CASCADE)
    fecha_atencion = models.DateField()
    inicio_atencion = models.DateField()
    final_atencion = models.DateField()
    estado = models.CharField(max_length=150)
    observaciones = models.TextField()

    def __str__(self):
        return f'{self.estado}{self.medico_id}{self.paciente_id}'


class Horario(CommonInfo):
    medico_id = models.ForeignKey(Medico, on_delete=models.CASCADE)
    fecha_atencion = models.DateField()
    inicio_atencion = models.DateField()
    final_atencion = models.DateField()

    def __str__(self):
        return f'{self.medico_id}{self.fecha_atencion}{self.paciente_id}'














